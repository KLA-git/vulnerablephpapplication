<!-- index.php - erste Seite bei Aufruf -->

<!-- include other php files-->
<?php include('headArea.php') ?>

<?php include('header.php') ?>

<main>
  <section class="py-5 text-center container">
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
        <h1 class="fw-light">Meme-Galerie</h1>
        <p class="lead text-muted">Hier gibt es wirklich super Memes zu sehen. Aber dafür müsst ihr euch erstmal registrieren. :)</p>
        <p>
          <!-- button link to register.php-->
          <a href="register.php" class="btn btn-primary my-2">Registrieren</a> <!-- registrieren button-->
        </p>
      </div>
    </div>
  </section>
</main>

<?php include('footer.php') ?>
