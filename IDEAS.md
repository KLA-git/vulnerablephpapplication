# Brainstorming Meme-Website
	
	/Grundfunktionen
*	 - User Accounts (mit Berechtigungsstufen, Verifizierung, etc.)
*	 - Meme Upload (als File oder URL)
*	 - Upvote / Downvote
	 - Meme Autoplay
 	 - Meme des Tages/Woche (basierend auf Votes)
*	 - Commentss
*	 - Meme Tags
	 - Template Sammlung
*	 - Design Upgrade
	 - DSGVO Konformitaet & Disclaimer

	/Tasks
	 - Backend (Datenbanken) -> Flo, Adrian (SQL), Christian, Tim
	 - Frontend (Design, CSS) -> Jana, Johannes
	 - Frontend (Funktion, HTML) -> Pascal, Adrian
	 - Administration (DSGVO, etc.) -> Tim

	/Upgrades
	 - Lehrer-Accounts (beschränkter Zugriff)
	 - Cookies, Tracker & Analytics
	 - Gender-Mode
	 - Design Randomizer
